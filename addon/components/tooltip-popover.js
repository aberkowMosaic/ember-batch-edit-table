import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({
  tagName: '',
  actions: {
    hidePopover(){
      $('#toggle-popover').click();
    }
  }
});
